package com.example.moviesapp.ui.movies

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.moviesapp.domain.movie.repository.MovieRepository
import com.example.moviesapp.domain.movie.retrofit.MovieDbApiProvider
import com.example.moviesapp.domain.movie.retrofit.RetrofitMovieDataSource
import com.example.moviesapp.domain.movie.room.AppDatabase
import com.example.moviesapp.domain.movie.room.RoomMovieDataSource
import com.example.moviesapp.utils.NetworkConnectivity

class MoviesViewModelFactory(private val context: Context): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(MovieRepository::class.java)
            .newInstance(MovieRepository( //do tuka imavme samo 1 dependency, a sega ke go dopolnime so novi
                RetrofitMovieDataSource(MovieDbApiProvider.getMovieDbApi()),
                RoomMovieDataSource(AppDatabase.getDatabase(context).movieDao()),
                NetworkConnectivity(context)
            )) //na ovoj nacin go kompletiravme constructor-ot na MovieRepository

    }
}