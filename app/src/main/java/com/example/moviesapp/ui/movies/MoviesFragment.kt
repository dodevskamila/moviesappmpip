package com.example.moviesapp.ui.movies

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.moviesapp.R
import com.example.moviesapp.adapters.MovieAdapter
import com.example.moviesapp.databinding.FragmentMoviesBinding
import com.google.android.material.snackbar.Snackbar

class MoviesFragment : Fragment(R.layout.fragment_movies) {

    private var _binding: FragmentMoviesBinding? = null
    private val binding get() = _binding!!

    private lateinit var moviesViewModel: MoviesViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentMoviesBinding.bind(view) //za da go povrzeme view-ot so klasata

//        moviesViewModel = ViewModelProvider(this).get(MoviesViewModel::class.java)
        //ama sega nema da moze vaka prosto da go instancirame VM bidejki imame konsruktor so argumenti vo VM
        //(pokazi vo VM)
        //zatoa ke go napravime so factory
        val viewModelFactory = MoviesViewModelFactory(requireContext())
        moviesViewModel = ViewModelProvider(this, viewModelFactory)[MoviesViewModel::class.java]

        var adapter : MovieAdapter = MovieAdapter() //inicijalno e prazen
        binding.list.adapter = adapter

        moviesViewModel.getMovieLiveData().observe(viewLifecycleOwner) {
            //koga ke se promeni lista od movies, ke se povika ova
            adapter.updateMovies(it) //go update-irame adapterot so novata lista od movies
//            adapter = MovieAdapter(ArrayList(it)) //go pravime adapterot so lista od movies
        }

        binding.button.setOnClickListener{
            val query = binding.editQuery.text.toString()
            if (query.isEmpty()) {
                Snackbar.make(view,R.string.please_enter_query, Snackbar.LENGTH_LONG).show()
            } else {
                moviesViewModel.search(query)
            }
        }
        moviesViewModel.listAll()

    }
}