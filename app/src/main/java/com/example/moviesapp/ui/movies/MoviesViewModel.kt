package com.example.moviesapp.ui.movies

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.moviesapp.domain.movie.model.Movie
import com.example.moviesapp.domain.movie.repository.MovieRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MoviesViewModel(private val movieRepository: MovieRepository): ViewModel() {
  //  ova view model na pocetok ke ima samo edna LiveData promenliva
// koja pri update soodvetno ke vrati feedback deka treba da se azurira UI, t.e. RecyclerView

    private val _movies = MutableLiveData<List<Movie>>()
    fun getMovieLiveData(): LiveData<List<Movie>> = _movies

    fun search(query: String){
        //do ovoj moment koristime korutini, i site dosegasni f-ii se suspend funkcii
        //sega ke naprvime povik za korutina so launch
        viewModelScope.launch(Dispatchers.IO) {//sakame da kreirame korutina na nivo na view model
            //vo vakov tip na pr zadolzitelno treba da go specificirame dispatcher-ot
            //bidejki ke imame blokiracki povici do nekoj nadvoresen resurs
            //i da go povikame queryMovies od repository
            //i da go zacuvame rezultatot vo _movies
            val movies = movieRepository.queryMovies(query)
            _movies.postValue(movies) // mora da koristime postValue bidejki moze da se naogjame na drug thread
            // mora vaka za live data da bide pravilno iskoristena
        }
    }

    fun listAll() { //kreiram nova korutina
        viewModelScope.launch(Dispatchers.IO) {//rekovme deka ovoj dispatcher e dobar za r/w operacii
            val movies = movieRepository.listMovies()
            _movies.postValue(movies) //ova live data ke go povika callbackot vo MoviesFragment koj ke go update-ira adapterot
            //so novite promeni od movies livedata
        }
    }
}