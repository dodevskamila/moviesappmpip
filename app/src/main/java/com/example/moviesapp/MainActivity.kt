package com.example.moviesapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import com.example.moviesapp.ui.movies.MoviesFragment

//https://api.themoviedb.org/3/movie/11?api_key=693ae0400591baf02c2c48001953c69d

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                add(R.id.fragment_container_view, MoviesFragment())
                setReorderingAllowed(true)
            }
        }
    }
}