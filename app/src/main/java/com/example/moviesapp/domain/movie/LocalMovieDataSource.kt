package com.example.moviesapp.domain.movie

import com.example.moviesapp.domain.movie.model.Movie

interface LocalMovieDataSource {
    //ke gi sodrzi site operacii koi treba da gi preprakjame do samata baza
    suspend fun insertMovie(movie: Movie)

    suspend fun saveAll(movies: List<Movie>) //da gi zememe site filmovi
    suspend fun delete(id: Int) //da izbriseme film

    suspend fun getAll(): List<Movie> //da gi zememe site filmovi

    suspend fun searchMovies(query: String): List<Movie> //da gi zememe site filmovi so daden naslov
}