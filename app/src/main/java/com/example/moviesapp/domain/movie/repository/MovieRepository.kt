package com.example.moviesapp.domain.movie.repository

import com.example.moviesapp.domain.movie.LocalMovieDataSource
import com.example.moviesapp.domain.movie.RemoteMovieDataSource
import com.example.moviesapp.domain.movie.model.Movie
import com.example.moviesapp.utils.NetworkConnectivity

class MovieRepository(
    private val remoteMovieDataSource: RemoteMovieDataSource,
    private val localMovieDataSource: LocalMovieDataSource,
    private val networkConnectivity: NetworkConnectivity
) {

    suspend fun queryMovies(query: String): List<Movie> {
        if (networkConnectivity.isNetworkAvailable) {
            return remoteMovieDataSource.search(query).apply { localMovieDataSource.saveAll(this) }
        }
        return localMovieDataSource.searchMovies(query)
    }
    //kazavme uste na prvoto predavanje deka Repository treba da sozdrzi cist Kotlin kod

    suspend fun listMovies(): List<Movie> {
        return localMovieDataSource.getAll()
    }
}