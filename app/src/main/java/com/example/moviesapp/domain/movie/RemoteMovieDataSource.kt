package com.example.moviesapp.domain.movie

import com.example.moviesapp.domain.movie.model.Movie

interface RemoteMovieDataSource {
    suspend fun search(query: String): List<Movie>
    //treba da go obrabotime toj response, da izvleceme lista da ja vratime nazad listata od filmovi
}