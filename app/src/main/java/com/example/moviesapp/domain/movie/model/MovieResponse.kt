package com.example.moviesapp.domain.movie.model

data class MovieResponse(val page: Int, val results: List<Movie>)