package com.example.moviesapp.domain.movie.room
import android.content.Context
import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.moviesapp.domain.movie.model.Movie

@Database(entities = [Movie::class], version = 1, exportSchema = false)
abstract class AppDatabase: RoomDatabase() {
    abstract fun movieDao(): MovieDao

    //static singleton object koj ke ja pretstavuva bazata na podatoci
    companion object {
        @Volatile //za da znae jvm deka ovaa instanca e vidliva na povekje threadovi
        //i moze da bide izmeneta od tie povekje threads
        private var INSTANCE: AppDatabase? = null
        //koristime singleton pattern za da imame samo edna instanca od bazata na podatoci,
        //t.e. da ne se kreiraat celo vreme novi sesii kon DB, ja iskoristuvame istata sesija
        const val DATABASE_NAME = "movies_db"

        @JvmStatic //oznaka za java deka e static
        fun getDatabase(context: Context): AppDatabase { //builder na singleton objektot
            return INSTANCE ?: synchronized(this) { //ako ne e null vrati go, inaku kreiraj go
                //za da izbegneme vo eden moment povekje threadovi da probaat da ja kreiraat
                //ovaa instanca, pravime sinhronizacija na kreiranjeto na instancata
                val instance = androidx.room.Room.databaseBuilder(
                    context.applicationContext, //kontekstot na aplikacijata
                    AppDatabase::class.java, // klasata koja ke ja koristime za da ja kreirame bazata
                    DATABASE_NAME //ime na bazata
                ).build()
                INSTANCE = instance //dodeluvame vrednost na instancata
                instance //singleton f-jata sekogas ke vrakja 1 instanca od bazata
            }
        }
    }
}