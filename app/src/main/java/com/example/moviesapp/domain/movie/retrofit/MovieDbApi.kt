package com.example.moviesapp.domain.movie.retrofit

import com.example.moviesapp.domain.movie.model.MovieResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieDbApi {

    //ke pratime get baranje do search/movie
    @GET("search/movie")
    //parametrite ovde gi prakjame kako query parametri
    suspend fun searchMovies(@Query("query") query: String): Response<MovieResponse>
}