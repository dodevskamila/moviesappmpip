package com.example.moviesapp.domain.movie.room

import androidx.room.*
import com.example.moviesapp.domain.movie.model.Movie

@Dao
interface MovieDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE) //sto ke se sluci ako imame konflikt,
    // t.e. ako dodavame 2 filma so isto id
    suspend fun insertMovie(movie: Movie)

    @Delete()
    suspend fun deleteMovie(movie: Movie) //da izbriseme film

    @Query("DELETE FROM movie where id = :id")
    suspend fun delete(id: Int) //da izbriseme film so odredeno id

    @Query("SELECT * FROM movie")
    suspend fun getAll(): List<Movie> //da gi zememe site filmovi

    @Query("SELECT * FROM movie WHERE title LIKE :query")
    suspend fun searchMovies(query: String): List<Movie> //da gi zememe site filmovi so daden naslov

    //impl nema da imamee tuka tuku Room avtomatski ke ja dodade impl na interfejsot
}