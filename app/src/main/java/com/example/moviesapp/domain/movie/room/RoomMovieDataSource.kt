package com.example.moviesapp.domain.movie.room

import com.example.moviesapp.domain.movie.LocalMovieDataSource
import com.example.moviesapp.domain.movie.model.Movie

class RoomMovieDataSource(private val movieDao: MovieDao): LocalMovieDataSource { //go injektirame MovieDao
    //ke napravime access do sao f-iite koi ke bidat ovozmozeni do MovieRepository-to
    override suspend fun insertMovie(movie: Movie) {
        movieDao.insertMovie(movie)
    }
    override suspend fun saveAll(movies: List<Movie>) {
        for (movie in movies) {
            movieDao.insertMovie(movie)
        }
    }
    override suspend fun delete(id: Int) {
        movieDao.delete(id)
    }
    override suspend fun getAll(): List<Movie> {
        return movieDao.getAll()
    }
    override suspend fun searchMovies(query: String): List<Movie> {
        return movieDao.searchMovies(query)
    }
}