package com.example.moviesapp.domain.movie.retrofit

import com.example.moviesapp.domain.movie.RemoteMovieDataSource
import com.example.moviesapp.domain.movie.model.Movie

class RetrofitMovieDataSource(private val movieDbApi: MovieDbApi): RemoteMovieDataSource {
    override suspend fun search(query: String): List<Movie> {
        val movieResponse = movieDbApi.searchMovies(query) // go prakjame ovoj parametar na api-to
        val responseBody = movieResponse.body() // go zemame body-to na response-ot
        if(movieResponse.isSuccessful && responseBody != null) { //ako ne e uspesen ili ako e null
           return responseBody.results //vrati go results od odgovorot
        }
        throw Exception("Error searching movies") //vo sprotivno frlame exception
    }
}