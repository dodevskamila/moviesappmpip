package com.example.moviesapp.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.moviesapp.R
import com.example.moviesapp.adapters.MovieAdapter.MovieViewHolder
import com.example.moviesapp.domain.movie.model.Movie

//ke se grizi za prikazuvanje na podatocite vo recyclerview
class MovieAdapter(private val movies: ArrayList<Movie> = ArrayList<Movie>()):RecyclerView.Adapter<MovieViewHolder>() {

    class MovieViewHolder(view: View): RecyclerView.ViewHolder(view){
        private var imageView: ImageView = view.findViewById(R.id.movie_image)
        private var titleText: TextView = view.findViewById(R.id.movie_title)

        fun bind(movie: Movie){
            //sega za da da bindneme slika od apito, t.e. od remote src ke koristime Glide
            //iako prethodniot pat koristevme Picasso, no i Glide e isto tolku popularen
            Glide.with(imageView)
                 .load("https://image.tmdb.org/t/p/w185${movie.posterPath}")//apsoluten path do slikata
                .centerCrop().placeholder(R.drawable.ic_launcher_background)//dobra praksa e da stavame placeholder
                .into(imageView)

            titleText.text = movie.title
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
       val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_movie, parent, false)
        return MovieViewHolder(view)
    }

    override fun getItemCount(): Int {
       return movies.size
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bind(movies[position]) //zemame film koj se naogja na odedena pozicija
    }

    fun updateMovies(newMovies: List<Movie>){
        movies.clear()
        movies.addAll(newMovies)
        notifyDataSetChanged() // za da se svesti recyclerview-ot deka ima promena vo podatocite i da se azurira
    }

}