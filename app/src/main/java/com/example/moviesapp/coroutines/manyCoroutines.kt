import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlin.system.measureTimeMillis

fun main() = runBlocking {
    val time = measureTimeMillis {
        testTime()
//        testTimeThread()
    }
    println("Completed in $time ms")//vreme za izvrsuvanje na kodot vo milisekundi
}

suspend fun testTime() = coroutineScope {
    repeat(100) {//ova znaci 100k korutini
        launch(Dispatchers.IO) {
            delay(1000)
            print(Thread.currentThread().name) //go pecatime imeto na threadot na koj se izvrsuva
        }
    }
}

////sega so klasicni threads
//fun testTimeThread() = run {
//    repeat(100_000) {
//        Thread {
//            Thread.sleep(1000)//za da threadot go zaspieme, t.e. vo korutini ova se vika suspend
//            print(Thread.currentThread().name)
//        }.start()
//    }
//}