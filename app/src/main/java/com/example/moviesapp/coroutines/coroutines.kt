package com.example.moviesapp.coroutines

import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
//
//fun main() = runBlocking {
//    launch {
//        delay(1000) //so ova go odlozuvame pecatenjeto na World
//        println("World!")
//    }
//    println("Hello")
//}

//za da izvrsime kod vo posebna korutina treba kodot da go stavime vo block launch
//fun main() = runBlocking {
//    launch {
//        delay(1000) //so ova go odlozuvame pecatenjeto na World
//        println("Hello")
//        println("World!")
//    }
//}

fun main() = runBlocking {
    launch {
        doWorld()
    }
}

suspend fun doWorld() = coroutineScope { //sega vekje ne go koristime
    //runBlocking tuku go koristime coroutineScope
    var job = launch {
        delay(1000)
        println("World!")
    }
    println("Hello")
    job.join() //sme napravile bazicna sinhronizacija
    println("Done!")
}